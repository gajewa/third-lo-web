package team.jit.thirdlo.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import team.jit.thirdlo.entity.Student;
import team.jit.thirdlo.service.StudentService;

@RestController
@RequestMapping("/api/student")
public class StudentController {

    private StudentService studentService;

    @Autowired
    public StudentController(StudentService studentService) {
        this.studentService = studentService;
    }

    @GetMapping
    public String hello(){
        return "hello world!";
    }

    @GetMapping("/findByStudentId/{studentId}")
    public Student findByStudentId(@PathVariable String studentId){
        return studentService.findByStudentId(studentId);
    }

    @GetMapping("/getAll")
    public List<Student> getAllStudents(){
        return studentService.getAll();
    }

    @PostMapping("/saveNew")
    public void saveNewStudent(@RequestBody Student student){
        studentService.saveNew(student);
    }

    @GetMapping("/getAllActive")
    public List<Student> findAllByActiveTrue(){
        return studentService.findAllByActiveTrue();
    }

    @DeleteMapping("/delete")
    public void deleteStudent(@RequestBody Student student){
        studentService.deleteStudent(student);
    }

    @PatchMapping("/update")
    public void updateStudent(@RequestBody Student student){
        studentService.updateStudent(student);
    }

}
