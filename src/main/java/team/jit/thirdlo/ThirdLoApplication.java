package team.jit.thirdlo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ThirdLoApplication {

	public static void main(String[] args) {
		SpringApplication.run(ThirdLoApplication.class, args);
	}

}
