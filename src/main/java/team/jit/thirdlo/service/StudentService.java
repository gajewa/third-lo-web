package team.jit.thirdlo.service;

import java.util.List;

import javax.persistence.EntityNotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import team.jit.thirdlo.entity.Student;
import team.jit.thirdlo.repository.StudentRepository;

@Service
public class StudentService {

    private StudentRepository studentRepository;

    @Autowired
    public StudentService(StudentRepository studentRepository) {
        this.studentRepository = studentRepository;
    }

    public List<Student> getAll(){
        return studentRepository.findAll();
    }

    public Student findByStudentId(String studentId){
        return studentRepository.findByStudentId(studentId)
                .orElseThrow(() -> new EntityNotFoundException("Student of student id " + studentId + " not found."));
    }

    public void saveNew(Student student) {
        studentRepository.save(student);
    }

    public List<Student> findAllByActiveTrue(){
        return studentRepository.findAllByActiveTrue();
    }
    public void deleteStudent(Student student){
        studentRepository.delete(student);
    }

    public void updateStudent(Student student) {
        studentRepository.save(student);
    }
}
